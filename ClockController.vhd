-----------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:   	02/02/2022 
-- Module Name:    	Clock Controller
-- Project Name: 	TFG_RandomPulses
-- Target Devices:	Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------

LIBRARY ieee;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity ClockController is
	
	port(
		clk_1 		: in std_logic;
		clk_2		: in std_logic;
		clk_3		: in std_logic;
		clk_4		: in std_logic;
		clk_5		: in std_logic;
		
		clk_select	: in std_logic_vector (2 downto 0);
		clk_out  	: out std_logic	
	);
	
end ClockController;

ARCHITECTURE behavioral OF ClockController IS

BEGIN

	with clk_select select clk_out <=
		clk_1 when "000",
		clk_2 when "001",
		clk_3 when "010",
		clk_4 when "011",
		clk_5 when "100",
		'0' when others;

end behavioral; 