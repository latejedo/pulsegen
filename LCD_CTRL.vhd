-----------------------------------------------------------------
-- Company:         UCM - GAE - Madrid
-- Engineers:       Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     LCD_CTRL
-- Project Name: 	TFG_RandomPulses
-- Target Devices:  Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------

LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity LCD_CTRL is	
	port(	
		clk 			: in std_logic;
		reset 			: in std_logic;
		cuenta  		: in unsigned (27 downto 0);
		counter_ready	: in std_logic;
		
		LCD0  : out std_logic_vector(6 downto 0);
		LCD1  : out std_logic_vector(6 downto 0);
		LCD2  : out std_logic_vector(6 downto 0);
		LCD3  : out std_logic_vector(6 downto 0)
	);
end LCD_CTRL;

architecture Behavioral of LCD_CTRL is

	signal bcd0 : std_logic_vector(3 downto 0);
	signal bcd1 : std_logic_vector(3 downto 0);
	signal bcd2 : std_logic_vector(3 downto 0);
	signal bcd3 : std_logic_vector(3 downto 0);
	signal bcd4 : std_logic_vector(3 downto 0);
	signal bcd5 : std_logic_vector(3 downto 0);
	signal bcd6 : std_logic_vector(3 downto 0);
	signal bcd7 : std_logic_vector(3 downto 0);
	signal bcd8 : std_logic_vector(3 downto 0);
	
	signal binary 		: unsigned(27 downto 0);
	signal number_ready : std_logic;
	signal busy 		: std_logic;
	signal scratch 		: unsigned(63 downto 0);
	
	type states is (waiting, convert, converting);
	signal LCD_CTRL_state : states := waiting; 
	
BEGIN	
	
	binary_bcd_inst : entity work.binary_bcd
		port map(
			clk => clk,
			reset => reset,
			binary_in => binary,
			number_ready => number_ready,
			busy => busy,
			scratch => scratch
		);
	
	bcd2segment0 : entity work.bcd2segment
		port map(
			clk => clk,
			reset => reset,
			numeros => bcd6,
			segmentos => LCD0
		);
		
	bcd2segment1 : entity work.bcd2segment
		port map(
			clk => clk,
			reset => reset,
			numeros => bcd5,
			segmentos => LCD1
		);
		
	bcd2segment2 : entity work.bcd2segment
		port map(
			clk => clk,
			reset => reset,
			numeros => bcd4,
			segmentos => LCD2
		);
		
	bcd2segment3 : entity work.bcd2segment
		port map(
			clk => clk,
			reset => reset,
			numeros => bcd3,
			segmentos => LCD3
		);

	PROCESS(clk, reset)
		
	BEGIN					
	--Vamos a hacer una maquina de estados que controle 
	--la entrada de numeros a los conversores
		
		if reset ='1' then
			number_ready <= '0';
			LCD_CTRL_state <= waiting;
		
		elsif rising_edge(clk) then
		
			CASE LCD_CTRL_state is
			
				when waiting =>
				
						number_ready <= '0';
						if counter_ready = '1' then
							binary <= cuenta;
							LCD_CTRL_state <= convert;
						else
							LCD_CTRL_state <= waiting;
						end if;	
						
				when convert =>
				
						number_ready <= '1';
						binary <= binary;
						if busy = '1' then
							LCD_CTRL_state <= converting;
						else
							LCD_CTRL_state <= convert;
						end if;
						
				when converting =>
				
						number_ready <= '0';
						binary <= binary;
						if busy = '0' then
							bcd8 <= STD_LOGIC_VECTOR(scratch(63 downto 60));
							bcd7 <= STD_LOGIC_VECTOR(scratch(59 downto 56));
							bcd6 <= STD_LOGIC_VECTOR(scratch(55 downto 52));
							bcd5 <= STD_LOGIC_VECTOR(scratch(51 downto 48));
							bcd4 <= STD_LOGIC_VECTOR(scratch(47 downto 44));
							bcd3 <= STD_LOGIC_VECTOR(scratch(43 downto 40));
							bcd2 <= STD_LOGIC_VECTOR(scratch(39 downto 36));
							bcd1 <= STD_LOGIC_VECTOR(scratch(35 downto 32));
							bcd0 <= STD_LOGIC_VECTOR(scratch(31 downto 28));
							
							LCD_CTRL_state <= waiting;

						else
							LCD_CTRL_state <= converting;
						end if;
				
				when others =>
						number_ready <= '0';
						LCD_CTRL_state <= waiting;
			
			end case;
		
		end if;
	
	END PROCESS;
		
end Behavioral;