----------------------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     RandomPulses
-- Project Name: 	TFG_RandomPulses
-- Target Devices:	Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity RandomPulses is

	generic(
		seed1 : std_logic_vector(15 downto 0);
		seed2 : std_logic_vector(15 downto 0);
		seed3 : std_logic_vector(15 downto 0)
	);
	
	port(
		CLK_IN	: in std_logic;
		reset 	: in std_logic;
		GeffeOut : out std_logic
	);
	
end RandomPulses;

ARCHITECTURE behavioral OF RandomPulses IS
	
	signal registro0 : STD_logic_vector(15 downto 0);
	signal registro1 : STD_logic_vector(15 downto 0);
	signal registro2 : STD_logic_vector(15 downto 0);
	signal feedback  : STD_LOGIC_VECTOR(2 downto 0);

	BEGIN

	PROCESS (CLK_IN, reset)
	BEGIN 
		
		IF reset = '1' then
			
			registro0 <= seed1;
			registro1 <= seed2;
			registro2 <= seed3;
			feedback <= (others =>'0');
			
		elsif rising_edge(CLK_IN) then
		
			feedback(0) <= registro0(14) XOR registro0(7) XOR registro0(3);
			registro0 (15 downto 0) <= feedback(0) & registro0(15 downto 1);
			
			feedback(1) <= registro1(14) XOR registro1(7) XOR registro1(3);
			registro1 (15 downto 0) <= feedback(1) & registro1(15 downto 1);

			feedback(2) <= registro2(14) XOR registro2(7) XOR registro2(3);
			registro2 (15 downto 0) <= feedback(2) & registro2 (15 downto 1);

			GeffeOut <= (feedback(0) AND feedback(1)) XOR (feedback(1) AND feedback(2)) XOR feedback(2); 
					
		end IF;
		
	end PROCESS;		

end behavioral; 