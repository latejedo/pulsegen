-------------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     Shaper
-- Project Name: 	TFG_RandomPulses
-- Target Devices:	Altera Cyclone V GX Starter Kit
-------------------------------------------------------------------------

LIBRARY ieee;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity Shaper is
	
	port(
		clk_50MHz 	: in std_logic;
		reset		: in std_logic;
		input		: in std_logic;
		output		: buffer std_logic

	);
	
end Shaper;

ARCHITECTURE behavioral OF Shaper IS
	signal last_input : std_logic;
	signal cuenta 	  : integer range 0 to 12;
	
BEGIN

	PROCESS(clk_50MHz,reset) 	

		BEGIN					
			if reset ='1' then	
				last_input <= '0';
				output <= '0';
				cuenta <= 0;
				
			elsif rising_edge(clk_50MHz) then
				last_input <= input;
				
				--Control Output
				if last_input = '0' and input = '1' then
					output <= '1';
				
				elsif cuenta = 9 then
					output <= '0';
					
				else
					output <= output;
				
				end if;
				
				--Control cuenta
				if output = '1' then
					cuenta  <= cuenta + 1;
				
				elsif output = '0' then
					cuenta <= 0;
				
				else
					cuenta <= cuenta;
				
				end if;
				
			end if;
		
	END PROCESS;

end behavioral; 