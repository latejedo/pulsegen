-----------------------------------------------------------------
-- Company:         UCM - GAE - Madrid
-- Engineers:       Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     SignalPulses
-- Project Name: 	TFG_RandomPulses
-- Target Devices:	Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------

LIBRARY ieee;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity SignalPulses is

	generic(
		seed : std_logic_vector(15 downto 0)
	);
	
	port(
		clk_50MHz	: in std_logic;	
		reset 	 	: in std_logic;
		Senal		: out std_logic
	);
	
end SignalPulses;


ARCHITECTURE behavioral OF SiganlPulses IS
	
	signal registro : std_logic_vector(15 downto 0);
	signal feedback : std_logic

	BEGIN

		Señal <= (feedback AND CTRL) OR push;

		SiganlPulses_Inst : entity work.RandomPulses

			generic map(
				seed => "0110110010101011"
			)
			
			port map(
				clk_50MHz 	=> CLOCK_50_B5B,
				reset 		=> KEY(0),
				feedback	=> feedback
			);	

end behavioral; 