----------------------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:    	02/10/2022 
-- Module Name:     TFG_RandomPulses_Top
-- Project Name: 	TFG_RandomPulses
-- Target Devices: 	Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------------------

LIBRARY ieee;
LIBRARY COUNT;
LIBRARY altera;

USE altera.altera_primitives_components.all;
USE COUNT.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity TFG_RandomPulses_Top is
	port(
		CLOCK_50_B5B 	: in std_logic;	
		CLOCK_50_B8A	: in std_logic;
		KEY 			: in std_logic_vector (2 downto 0);
		LEDG 			: out std_logic_vector (7 downto 0);
		LEDR 			: out std_logic_vector (9 downto 0);
		SW 				: in std_logic_vector (9 downto 0);
		GPIO 			: inout std_logic_vector (21 downto 0);
		
		LCD0 			: out std_logic_vector (6 downto 0);
		LCD1 			: out std_logic_vector (6 downto 0);
		LCD2 			: out std_logic_vector (6 downto 0);
		LCD3 			: out std_logic_vector (6 downto 0)
	);
	
end TFG_RandomPulses_Top;


ARCHITECTURE behavioral OF TFG_RandomPulses_Top IS
	
	signal reset			: std_logic;
	signal locked1			: std_logic;
	signal locked2			: std_logic;
	signal intern_clk 		: std_logic;
	signal clk_noise1		: std_logic;
	signal clk_noise2		: std_logic;
	signal clk_signal		: std_logic;
	signal clk_select 		: std_logic_vector (5 downto 0);
	
	signal clk_pps			: std_logic;
	signal cuenta 			: unsigned (27 downto 0);
	signal cuenta1 			: unsigned (27 downto 0);
	signal cuenta2 			: unsigned (27 downto 0);
	signal cuenta3 			: unsigned (27 downto 0);
	signal counter_out 		: std_logic_vector(27 downto 0);
	signal counter_ready 	: std_logic;
	signal counter_ready1 	: std_logic;
	signal counter_ready2	: std_logic;
	signal counter_ready3	: std_logic;
	signal pps 				: std_logic;
	
	signal shaperin			: std_logic;
	signal geffe1			: std_logic;
	signal geffe2			: std_logic;
	signal geffe3			: std_logic;
	signal TIB1				: std_logic;
	signal TIB2				: std_logic;
	signal noise1			: std_logic;
	signal noise2			: std_logic;
	signal signal_out		: std_logic;
	
	signal clk_12kHz		: std_logic;
	signal clk_25kHz		: std_logic;
	signal clk_30kHz 	 	: std_logic;
	signal clk_40khz		: std_logic;
	signal clk_50kHz 		: std_logic;
	signal clk_75kHz 		: std_logic;

	signal clk_250kHz		: std_logic;
	signal clk_500kHz		: std_logic;
	signal clk_1200kHz		: std_logic;
	signal clk_3MHz 		: std_logic;
	signal clk_5MHz 		: std_logic;
	signal clk_10MHz 		: std_logic;
	
	signal clk2_250kHz		: std_logic;
	signal clk2_500kHz		: std_logic;
	signal clk2_1200kHz		: std_logic;
	signal clk2_3MHz 		: std_logic;
	signal clk2_5MHz 		: std_logic;
	signal clk2_10MHz 		: std_logic;
	
BEGIN

	GPIO(4) 		 <= clk_noise1;
	GPIO(5)			 <= TIB1;
	GPIO(6)			 <= TIB2;
	LEDR(0) 		 <= pps;
	LEDR(1)			 <= counter_ready;
	LEDR(9 downto 2) <= (others => '0');
	LEDG(0) 		 <= locked1;
	LEDG(1)			 <= locked2;
	LEDG(2) 		 <= NOT KEY(1);
	LEDG(3) 		 <= NOT KEY(2);
	LEDG(7 downto 4) <= (others => '0');
	reset 			 <= NOT KEY(0);
	clk_select 		 <= SW(5 downto 0);
	
	contador_pps_inst : entity work.contador_pps
		port map(
			clk_pps	=> clk_1200kHz,
			reset	=> reset,
			pps 	=> pps
		);	
	
	clkgen_inst : entity work.clkgen
		port map(
		
			clk_50MHz 		=> CLOCK_50_B5B,
			clk_50MHzU20	=> CLOCK_50_B8A,
			reset			=> reset,
			locked1 		=> locked1,
			locked2			=> locked2,
			
			clk_12kHz 		=> clk_12kHz,
			clk_25kHz 		=> clk_25kHz,
			clk_40kHz		=> clk_40kHz,
			clk_50kHz 		=> clk_50kHz,
			clk_75kHz 		=> clk_75kHz,
			
			clk_250kHz		=> clk_250kHz,
			clk_500kHz		=> clk_500kHz,
			clk_1200kHz 	=> clk_1200kHz,
			clk_3MHz 		=> clk_3MHz,
			clk_5MHz 		=> clk_5MHz,
			clk_10MHz 		=> clk_10MHz,
			
			clk2_250kHz		=> clk2_250kHz,
			clk2_500kHz		=> clk2_500kHz,
			clk2_1200kHz 	=> clk2_1200kHz,
			clk2_3MHz 		=> clk2_3MHz,
			clk2_5MHz 		=> clk2_5MHz,
			clk2_10MHz 		=> clk2_10MHz
		);
		
	ClockController_noise : entity work.ClockController
		port map(
			clk_1 		=> clk_5MHz,
			clk_2 		=> clk_3MHz,
			clk_3		=> clk_1200kHz,
			clk_4 		=> clk_500kHz,
			clk_5 		=> clk_250kHz,
			clk_out 	=> clk_noise1,
			clk_select	=> clk_select(2 downto 0)
		);
		
	ClockController_noise_2 : entity work.ClockController
		port map(
			clk_1 		=> clk2_5MHz,
			clk_2 		=> clk2_3MHz,
			clk_3		=> clk2_1200kHz,
			clk_4 		=> clk2_500kHz,
			clk_5 		=> clk2_250kHz,
			clk_out 	=> clk_noise2,
			clk_select	=> clk_select(2 downto 0)
		);
		
	ClockController_signal : entity work.ClockController
		port map(
			clk_1		=> clk_75kHz,
			clk_2		=> clk_50kHz,
			clk_3		=> clk_40kHz,
			clk_4 		=> clk_25kHz,
			clk_5 		=> clk_12kHz,
			clk_out 	=> clk_signal,
			clk_select	=> clk_select(5 downto 3)
		);

	RandomPulses_Inst_1 : entity work.RandomPulses

		generic map(
			seed1 => "0110110010101011",
			seed2 => "0100110010101011",
			seed3 => "0110110011101011"
		)
		
		port map(
			CLK_IN		=> clk_noise1,
			reset 		=> reset,
			GeffeOut 	=> geffe1
		);
		
	Shaper_inst_1 : entity work.Shaper
		port map(
			clk_50MHz 	=> CLOCK_50_B5B,
			reset 		=> reset,
			input 		=> geffe1,
			output 		=> noise1
		);
		
	countcontroller_inst_1 : entity work.countcontroller
		port map(
			clk_50MHz 		=> CLOCK_50_B5B,
			reset			=> reset,
			pps				=> pps,
			geffe    		=> noise1,
			cuenta			=> cuenta1,
			counter_ready	=> counter_ready1		
		);
		
	RandomPulses_Inst2 : entity work.RandomPulses

		generic map(
			seed1 => "0001110010110011",
			seed2 => "0010010110100001",
			seed3 => "1011010010001101"
		)
		
		port map(
			CLK_IN 		=> clk_noise2,
			reset 		=> reset,
			GeffeOut 	=> geffe2
		);
		
	Shaper_inst_2 : entity work.Shaper
		port map(
			clk_50MHz 	=> CLOCK_50_B5B,
			reset 		=> reset,
			input 		=> geffe2,
			output 		=> noise2
		);
		
	countcontroller_inst_2 : entity work.countcontroller
		port map(
			clk_50MHz 		=> CLOCK_50_B5B,
			reset			=> reset,
			pps				=> pps,
			geffe    		=> noise2,
			cuenta			=> cuenta2,
			counter_ready	=> counter_ready2		
		);

	RandomPulses_Inst3_SIGNAL : entity work.RandomPulses

		generic map(
			seed1 => "0001110010110011",
			seed2 => "0010010110100001",
			seed3 => "1011010010001101"
		)
		
		port map(
			CLK_IN 		=> clk_signal,
			reset 		=> reset,
			GeffeOut 	=> geffe3
		);
		
	Shaper_inst_3 : entity work.Shaper
		port map(
			clk_50MHz 	=> CLOCK_50_B5B,
			reset 		=> reset,
			input 		=> geffe3,
			output 		=> signal_out
		);
		
	countcontroller_inst_3 : entity work.countcontroller
		port map(
			clk_50MHz 		=> CLOCK_50_B5B,
			reset			=> reset,
			pps				=> pps,
			geffe    		=> signal_out,
			cuenta			=> cuenta3,
			counter_ready	=> counter_ready3		
		);

	TIB1 <= signal_out or noise1;
	TIB2 <= signal_out or noise2;
	
	LCD_CTRL_inst : entity work.LCD_CTRL
		port map(
			clk 	=> CLOCK_50_B5B,
			reset	=> reset,
			counter_ready => counter_ready,
			cuenta 	=> cuenta,
			LCD0 	=> LCD3,
			LCD1 	=> LCD2,
			LCD2 	=> LCD1,
			LCD3 	=> LCD0
		);
		
	signal_2_LCD_inst : entity work.signal_2_LCD
		port map(
			SW => SW(9 downto 8),
			counter_ready => counter_ready,
			counter_ready1 => counter_ready1,
			counter_ready2 => counter_ready2,
			counter_ready3 => counter_ready3,
			cuenta 		=> cuenta,
			cuenta1 	=> cuenta1,
			cuenta2 	=> cuenta2,
			cuenta3 	=> cuenta3

		);
		
end behavioral; 