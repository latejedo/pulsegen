----------------------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:    	02/02/2022 
-- Module Name:    	bcd2segment
-- Project Name: 	TFG_RandomPulses
-- Target Devices:	Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------------------

LIBRARY ieee;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity bcd2segment is
	port(	
		clk 		: in std_logic; 
		reset 		: in std_logic;
		numeros  	: 	in std_logic_vector(3 downto 0);
		segmentos  	: 	out std_logic_vector(6 downto 0)
	);
		
end bcd2segment;

architecture Behavioral of bcd2segment is 
begin
	process (clk, reset)
	begin 
			
		IF reset = '1' then
				
		segmentos <= "0000110";
				
		elsif rising_edge(clk) then
			
			case numeros is
				
				when "0000" => 
					segmentos <=  "1000000"; --0
				when "0001" => 
					segmentos <=  "1111001"; --1
				when "0010" => 
					segmentos <=  "0100100"; --2
				when "0011" => 
					segmentos <=  "0110000"; --3
				when "0100" => 
					segmentos <=  "0011001"; --4 
				when "0101" => 
					segmentos <=  "0010010"; --5
				when "0110" => 
					segmentos <=  "0000010"; --6
				when "0111" => 
					segmentos <=  "1111000"; --7
				when "1000" => 
					segmentos <=  "0000000"; --8
				when "1001" => 
					segmentos <=  "0010000"; --9
				when others => 
					segmentos <=  "0000110"; --E
					
			end case;
				
		end IF;
	end process;	
end Behavioral;