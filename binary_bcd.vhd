--------------------------------------------------------------------
-- Company: 		UCM
-- Engineers: 		Luis Angel Tejedor (Double Dabble) 
-- Modificado por: 	Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:    	10/02/2023
-- Module Name:    	binary_bcd
-- Project Name: 	TFG_RandomPulses
-- Target Devices:  Altera Cyclone V GX Starter Kit
--------------------------------------------------------------------
-- This is just an implementation of double dabble algorithm 
-- to change from binary to BCD
-- https://en.wikipedia.org/wiki/Double_dabble
--------------------------------------------------------------------

library IEEE;

USE IEEE.std_logic_1164.all;
USE IEEE.NUMERIC_STD.ALL;
 
ENTITY binary_bcd is
    GENERIC(
    	N: positive := 28
        );
    PORT(
        clk: in STD_LOGIC;
        reset: in std_logic;
        number_ready: in STD_LOGIC;
        binary_in: in unsigned (N-1 downto 0);
        busy : out STD_LOGIC;
		  scratch : buffer unsigned (63 downto 0)
    );
END binary_bcd ;
 
ARCHITECTURE Behavioral OF binary_bcd IS

-- Counter of iterations. N needed. 0 to N-1. 
-- Using six bits to be able to count until 2N and more

signal cntr : UNSIGNED(6 downto 0) := (others => '0');

BEGIN

PROCESS(clk)

BEGIN
	if reset = '1' then
		cntr <= to_unsigned(2*N,7);
    	scratch(63 downto N) <= (others => '0');
		scratch(N-1 downto 0) <= (others => '0');
      busy <= '0';
    
    elsif rising_edge(clk) then
		if number_ready = '1' then
			cntr <= (others => '0');
			scratch(63 downto N) <= (others => '0');
			scratch(N-1 downto 0) <= binary_in;
         busy <= '1';
		else
			if cntr < 2*N-1 then
            	if cntr(0) = '0' then -- Si par
							scratch <= scratch sll 1;
							cntr <= cntr + 1;
							busy <= '1';
                else -- Si impar
                    if scratch(31 downto 28) > 4 then
                        scratch(31 downto 28) <= scratch(31 downto 28) + 3;
                    end if;
                    if scratch(35 downto 32) > 4 then
                        scratch(35 downto 32) <= scratch(35 downto 32) + 3;
                    end if;
                    if scratch(39 downto 36) > 4 then
                        scratch(39 downto 36) <= scratch(39 downto 36) + 3;
                    end if;
                    if scratch(43 downto 40) > 4 then
                        scratch(43 downto 40) <= scratch(43 downto 40) + 3;
                    end if;
                    if scratch(47 downto 44) > 4 then
                        scratch(47 downto 44) <= scratch(47 downto 44) + 3;
                    end if;
                    if scratch(51 downto 48) > 4 then
                        scratch(51 downto 48) <= scratch(51 downto 48) + 3;
                    end if;
                    if scratch(55 downto 52) > 4 then
                        scratch(55 downto 52) <= scratch(55 downto 52) + 3;
                    end if;
                    if scratch(59 downto 56) > 4 then
                        scratch(59 downto 56) <= scratch(59 downto 56) + 3;
                    end if;
                    if scratch(63 downto 60) > 4 then
                        scratch(63 downto 60) <= scratch(63 downto 60) + 3;
                    end if;                
						  
						  cntr <= cntr + 1;
                	  busy <= '1';
                end if;
			else
            	busy <= '0';
         end if;
		end if;
	end if;
END PROCESS;
END Behavioral;
