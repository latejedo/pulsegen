----------------------------------------------------------------------------------
-- Company: UCM - GAE - Madrid
-- Engineers: Alejandro Gutiérrez Llorente
-- <alegut06@ucm.es>
-- 
-- Create Date:    02/02/2022 
-- Module Name:    Clock Generator
-- Project Name: 	 TFG_RandomPulses
-- Target Devices: Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------------------

LIBRARY ieee;
LIBRARY PLL;

USE PLL.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity clkgen is
	
	port(
		clk_50MHz 		: in std_logic;
		clk_50MHzU20	: in std_logic;
		reset			: in std_logic;
		locked1 		: out std_logic;
		locked2 		: out std_logic;

		clk_12kHz		: out std_logic;
		clk_25kHz 		: out std_logic;
		clk_30kHz 		: out std_logic;  -- No se usa
		clk_40khz		: out std_logic;
		clk_50kHz 		: out std_logic;
		clk_75kHz 		: out std_logic;

		clk_250kHz		: out std_logic;		
		clk_500kHz		: out std_logic;
		clk_1200kHz 	: buffer std_logic;
		clk_3MHz 		: buffer std_logic;
		clk_5MHz 		: buffer std_logic;
		clk_10MHz 		: buffer std_logic;
		
		clk2_250kHz		: out std_logic;
		clk2_500kHz		: buffer std_logic;
		clk2_1200kHz 	: buffer std_logic;
		clk2_3MHz 		: buffer std_logic;
		clk2_5MHz 		: buffer std_logic;
		clk2_10MHz 		: buffer std_logic  -- No se usa 		
	);
	
end clkgen;

ARCHITECTURE behavioral OF clkgen IS

BEGIN

PLL_0 : entity PLL.PLL
	port map(
		refclk 	 => clk_50MHz,
		rst 	 => reset,
		outclk_0 => clk_1200kHz,
		outclk_1 => clk_3MHz,
		outclk_2 => clk_5MHz,
		outclk_3 => clk_10MHz,
		locked 	 => locked1
	);

PLL_1 : entity PLL.PLL
	port map(
		refclk 	 => clk_50MHzU20,
		rst 	 => reset,
		outclk_0 => clk2_1200kHz,
		outclk_1 => clk2_3MHz,
		outclk_2 => clk2_5MHz,
		outclk_3 => clk2_10MHz,
		locked 	 => locked2
	);

divider_500kHz : entity work.divider
	generic map (
		
		div_val	=>	100
	)
	port map (
		clk_in 	=> clk_5MHz,
		reset 	=> reset,
		clk_out => clk_500kHz
	
	);	

divider_2_500kHz : entity work.divider
	generic map (
		
		div_val	=>	100
	)
	port map (
		clk_in 	=> clk2_5MHz,
		reset 	=> reset,
		clk_out => clk2_500kHz
	
	);	
	
divider_250kHz : entity work.divider
	generic map (
		
		div_val	=>	200
	)
	port map (
		clk_in 	=> clk_5MHz,
		reset 	=> reset,
		clk_out => clk_250kHz
	
	);

divider_2_250kHz : entity work.divider
	generic map (
		
		div_val	=>	200
	)
	port map (
		clk_in 	=> clk2_5MHz,
		reset 	=> reset,
		clk_out => clk2_250kHz
	
	);
	
divider_75kHz : entity work.divider
	generic map (
		
		div_val	=>	66
	)
	port map (
		clk_in 	=> clk_5MHz,
		reset 	=> reset,
		clk_out => clk_75kHz
	
	);
	
divider_50kHz : entity work.divider
	generic map (
		
		div_val	=>	100
	)
	port map (
		clk_in 	=> clk_5MHz,
		reset 	=> reset,
		clk_out => clk_50kHz
	
	);

divider_40kHz : entity work.divider
	generic map (
		
		div_val	=>	30
	)
	port map (
		clk_in 	=> clk_1200kHz,
		reset 	=> reset,
		clk_out => clk_40kHz

	
	);	
	
divider_30kHz : entity work.divider
	generic map (
		
		div_val	=>	100
	)
	port map (
		clk_in 	=> clk_3MHz,
		reset 	=> reset,
		clk_out => clk_30kHz

	
	);	
	
divider_25kHz : entity work.divider
	generic map (
		
		div_val	=>	200
	)
	port map (
		clk_in 	=> clk_5MHz,
		reset 	=> reset,
		clk_out => clk_25kHz
	
	);

divider_12kHz : entity work.divider
	generic map (
		
		div_val	=>	100
	)
	port map (
		clk_in 	=> clk_1200kHz,
		reset 	=> reset,
		clk_out => clk_12kHz
	
	);

end behavioral; 