--------------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     Contador PPS
-- Project Name: 	TFG_RandomPulses
-- Target Devices:  Altera Cyclone V GX Starter Kit
---------------------------------------------------------------------------

LIBRARY ieee;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity contador_pps is
	
	port(
		clk_pps 	: in std_logic;
		reset		: in std_logic;
		pps 		: buffer std_logic
	);
	
end contador_pps;

ARCHITECTURE behavioral OF contador_pps IS

	signal cnt_out	: std_logic_vector (24 downto 0);
	signal cuenta 	: integer range 0 to 600000;

BEGIN

	PROCESS(clk_pps, reset)
			
		BEGIN					
			
			if reset ='1' then
				
				pps <= '0';
				cuenta <= 0;
			
			elsif rising_edge(clk_pps) then
				
				if cuenta = 599999 then
					pps <= not pps;
					cuenta <= 0;
				
				else
					pps <= pps;
					cuenta <= cuenta + 1;
				
				end if;
				
			end if;
		
	END PROCESS;
	
end behavioral;