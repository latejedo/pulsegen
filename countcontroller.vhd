-----------------------------------------------------------------------
-- Company: 		UCM - GAE - Madrid
-- Engineers: 		Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:     02/02/2022 
-- Module Name:     Count controller
-- Project Name: 	TFG_RandomPulses
-- Target Devices:  Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------

LIBRARY ieee;
LIBRARY COUNT;

USE COUNT.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity countcontroller is
	
	port(
		clk_50MHz 		: in std_logic;
		reset			: in std_logic;
		pps				: in std_logic;
		geffe    		: in std_logic;
		cuenta			: buffer unsigned (27 downto 0);
		counter_ready	: out std_logic
	);
	
end countcontroller;

ARCHITECTURE behavioral OF countcontroller IS

	signal last_pps		: std_logic;
	signal cnt_en		: std_logic;
	signal clear		: std_logic;
	signal q			: std_logic_vector(27 downto 0);

	type states is (counting, reseting, countupdate, countready);
	signal countcontroller_state : states := counting; 

BEGIN

COUNT_inst : entity COUNT.COUNT
	port map(
		aclr		=> clear,
		clock		=> geffe,
		cnt_en	=> cnt_en,
		q			=> q
		
	);
	
PROCESS(clk_50MHz,reset) 	
	BEGIN	

		if reset ='1' then
			cnt_en <= '0';
			clear <= '1';
			cuenta <= (others => '0');
			counter_ready <= '0'; 
			countcontroller_state <= reseting;
		
		elsif rising_edge(clk_50MHz) then
		
			last_pps <= pps;
		
			CASE countcontroller_state is
			
				when counting =>
				
					cnt_en <= '1';
					clear <= '0';
					cuenta <= cuenta;
					
					if last_pps = '0' and pps = '1' then		
						cnt_en <= '0';
						countcontroller_state <= countupdate;														
					else
						countcontroller_state <= counting;						
					end if;
				
				when countupdate =>
					
					cnt_en <= '0';
					clear <= '0';
					cuenta <= unsigned(q);
					counter_ready <= '0';
					countcontroller_state <= countready;
					
				when countready =>
				
					cnt_en <= '0';
					clear <= '0';
					cuenta <= cuenta;
					counter_ready <= '1';
					countcontroller_state <= reseting;
					
				when reseting =>
				
					cnt_en <= '0';
					clear <= '1';
					cuenta <= cuenta;
					counter_ready <= '0';
					countcontroller_state <= counting;
				
				when others =>
					countcontroller_state <= reseting;	
			
			end case;
		
		end if;
	
END PROCESS;

end behavioral; 