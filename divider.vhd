------------------------------------------------------------------
-- Company: UCM - GAE - Madrid
-- Engineers: Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:    02/02/2022 
-- Module Name:    Clock Controller
-- Project Name: 	 TFG_RandomPulses
-- Target Devices: Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------

library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity divider is
	Generic(
		div_val	: integer := 1000
	);
  
  Port ( 
		clk_in 	: in  STD_LOGIC;
		reset		: in std_logic;
    clk_out	: buffer STD_LOGIC
	);
end divider;

architecture Behavioral of divider is
  signal counter : integer range 0 to div_val;

  begin
    process(clk_in)
    begin

      if reset ='1' then
        
        counter <= 0;
        clk_out <= '0';
        
      elsif rising_edge(clk_in) then

        if counter < (div_val/2)-1 then
          counter <= counter + 1;
        else 
          counter <= 0;
          clk_out <= not clk_out;
        end if;

      end if;

    end process;
end Behavioral;
