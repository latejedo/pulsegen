----------------------------------------------------------------------------------
-- Company: 		 UCM - GAE - Madrid
-- Engineers: 		 Alejandro Gutiérrez Llorente <alegut06@ucm.es>
-- Create Date:    02/10/2022 
-- Module Name:    signal_2_LCD
-- Project Name: 	 TFG_RandomPulses
-- Target Devices: Altera Cyclone V GX Starter Kit
-----------------------------------------------------------------------------------
LIBRARY ieee;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity signal_2_LCD is
	port(
		counter_ready	: out std_logic;
		counter_ready1	: in std_logic;
		counter_ready2	: in std_logic;
		counter_ready3	: in std_logic;

		cuenta  : out unsigned (27 downto 0);
		cuenta1 : in unsigned (27 downto 0);
		cuenta2 : in unsigned (27 downto 0);
		cuenta3 : in unsigned (27 downto 0);

		SW : in std_logic_vector (1 downto 0)
	);
	
end signal_2_LCD;

ARCHITECTURE behavioral OF signal_2_LCD IS
	
BEGIN
				
	counter_ready <= 
		counter_ready1 when SW(1 downto 0) = "00" else 
		counter_ready2 when SW(1 downto 0) = "01" else
		counter_ready3 when SW(1 downto 0) = "10" else
		'0'	when SW(1 downto 0) = "11";
					
	cuenta <= 
		cuenta1 when SW(1 downto 0) = "00" else 
		cuenta2 when SW(1 downto 0) = "01" else
		cuenta3 when SW(1 downto 0) = "10" else
		to_unsigned(0, 28) when SW(1 downto 0) = "11";

end behavioral; 